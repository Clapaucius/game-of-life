﻿using UnityEngine;
using System.Collections;

public class Rules
{
    public static bool[] birthRules = new bool[9];
    public static bool[] sustainRules = new bool[9];
 
    public void ChangeBirthRule(int number)
    {
        birthRules[number] = !birthRules[number];
    }

    public void ChangeSustainRule(int number)
    {
        sustainRules[number] = !sustainRules[number];
    }
}
