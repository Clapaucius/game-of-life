﻿using UnityEngine;
using System.Collections;
using System;

public class Cell : MonoBehaviour
{
    [SerializeField] private Color _liveColor;
    [SerializeField] private Color _deadColor;
    private int _numberOfNeighbours;
    
    private InitialStateGenerator _initialStateGenerator;
    private SpriteRenderer _spriteRenderer;

    private bool _isAlive;

    private void Awake()
    {
        _initialStateGenerator = FindObjectOfType<InitialStateGenerator>();
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void OnMouseDown()
    {
        if (!_initialStateGenerator.HasGameBegun)
        {
            ChangeState();
        }
    }

    public void CountNeighbours(int x, int y)
    {
        _numberOfNeighbours = _isAlive ? -1 : 0;

        int xTemp;
        int yTemp;
        
        for (int i = x - 1; i <= x + 1; i++)
        {
            for (int j = y - 1; j <= y + 1; j++)
            {
                xTemp = i;
                yTemp = j;

                if (i < 0)
                {
                    xTemp = _initialStateGenerator.width - 1;
                }
                else if (i > _initialStateGenerator.width - 1)
                {
                    xTemp = 0;
                }

                if (j < 0)
                {
                    yTemp = _initialStateGenerator.height - 1;
                }
                else if (j > _initialStateGenerator.height - 1)
                {
                    yTemp = 0;
                }

                if (_initialStateGenerator.cells[xTemp, yTemp].GetComponent<Cell>()._isAlive)
                {
                    _numberOfNeighbours++;
                }        
            }
        }
    }

    public void DetermineState()
    {
        if (_isAlive)
        {
            for (var i = 0; i < 9; i++)
            {
                if (!Rules.sustainRules[i] && _numberOfNeighbours == i)
                {
                    SetDead();
                    return;
                }
            }
        }
        else
        {
            for (var i = 0; i < 9; i++)
            {
                if (Rules.birthRules[i] && _numberOfNeighbours == i)
                {
                    SetAlive();
                    return;
                }
            }
        }
    }

    private void ChangeState()
    {
        if (_isAlive)
        {
            SetDead();
        }
        else
        {
            SetAlive();
        }
    }

    public void SetAlive()
    {
        _isAlive = true;
        _spriteRenderer.color = _liveColor;
    }

    public void SetDead()
    {
        _isAlive = false;
        _spriteRenderer.color = _deadColor;
    }
}
