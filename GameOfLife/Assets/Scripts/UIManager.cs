﻿
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using TMPro;

public class UIManager : MonoBehaviour
{

    public Button[] birthRulesButtons;
    public Button[] sustainRulesButtons;
    public Slider delaySlider;
    public Slider chanceSlider;

    public TextMeshProUGUI delayText;
    public TextMeshProUGUI livingCellChanceText;
    public TextMeshProUGUI generationNumberValue;

    public Color darkButtonsColor;
    public Color lightButtonsColor;

    private Rules _rules;
    private InitialStateGenerator _initialStateGenerator;
    private IterationHandler _iterationHandler;

    private int _generationNumber;

    private void Awake()
    {
        _iterationHandler = GetComponent<IterationHandler>();
        _initialStateGenerator = GetComponent<InitialStateGenerator>();
        _rules = new Rules();
    }

    private void Start()
    {
        SetButtons();
        _iterationHandler.IterationAction += SetGenerationBannerValue;
        
        SetDelay(delaySlider.value);
        SetChanceOfAliveCell(chanceSlider.value);
        SetGenerationBannerValue();
        
        ChoseDefaultRules();
    }

    private void SetGenerationBannerValue()
    {
        _generationNumber++;
        generationNumberValue.text = $"Generation {_generationNumber}";
    }

    public void StartGame()
    {
        _initialStateGenerator.StartLivingProcess();
    }

    private void ChoseDefaultRules()
    {
        birthRulesButtons[3].onClick.Invoke();
        sustainRulesButtons[2].onClick.Invoke();
        sustainRulesButtons[3].onClick.Invoke();
    }

    private void StopGame()
    {
        _initialStateGenerator.StopLivingProcess();
    }

    private void SetButtons()
    {
        for (int i = 0; i < 9; i++)
        {
            int tempIntVar = i;
            birthRulesButtons[i].onClick.AddListener(() => _rules.ChangeBirthRule(tempIntVar));
            birthRulesButtons[i].onClick.AddListener(() =>
                ChangeButtonColor(birthRulesButtons[tempIntVar], Rules.birthRules[tempIntVar]));
            sustainRulesButtons[i].onClick.AddListener(() => _rules.ChangeSustainRule(tempIntVar));
            sustainRulesButtons[i].onClick.AddListener(() =>
                ChangeButtonColor(sustainRulesButtons[tempIntVar], Rules.sustainRules[tempIntVar]));
        }
    }

    private void ChangeButtonColor(Button button, bool isButtonPressed)
    {
        button.image.color = isButtonPressed ? darkButtonsColor : lightButtonsColor;
    }

    public void SetDelay(float delay)
    {
        delayText.text = "Time between iterations: "
                         + delay.ToString("F2") + " sec";
        _iterationHandler.delayBetweenIterations = delay;
    }
    
    public void SetChanceOfAliveCell(float chance)
    {
        livingCellChanceText.text = "Chance " + (chance * 100).ToString("F0") + "%";
        _initialStateGenerator.chanceOfAliveCell = chance;
    }

    public void SetAllCellsDead()
    {
        StopGame();
        _generationNumber = 0;
        SetGenerationBannerValue();
        _initialStateGenerator.SetAllCellsDead();
    }
    public void RandomInstantiatingButton()
    {
        _generationNumber = 0;
        _initialStateGenerator.SetCellStatesRandomly();
    }
}