﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class IterationHandler : MonoBehaviour
{
    public Action IterationAction;
    
    [Range(0.01f, 5f)]
    [HideInInspector] public float delayBetweenIterations;

    private InitialStateGenerator _initialStateGenerator;
    private float _timer;

    private void Awake()
    {
        _initialStateGenerator = GetComponent<InitialStateGenerator>();
    }

    private void Update()
    {
        TryToPerfomNextIteration();
    }

    private void TryToPerfomNextIteration()
    {
        _timer += Time.deltaTime;
        if (_initialStateGenerator.HasGameBegun && _timer >= delayBetweenIterations)
        {
            _timer = 0;
            PerfomIteration();
        }
    }

    private void PerfomIteration()
    {
        CountAllNeighbours();
        DetermineAllStates();
        IterationAction?.Invoke();

    }

    private void DetermineAllStates()
    {
        for (int i = 0; i < _initialStateGenerator.width; i++)
        {
            for (int j = 0; j < _initialStateGenerator.height; j++)
            {
                _initialStateGenerator.cells[i, j].GetComponent<Cell>().DetermineState();
            }
        }
    }

    private void CountAllNeighbours()
    {
        for (int i = 0; i < _initialStateGenerator.width; i++)
        {
            for (int j = 0; j < _initialStateGenerator.height; j++)
            {
                _initialStateGenerator.cells[i, j].GetComponent<Cell>().CountNeighbours(i, j);
            }
        }
    }
}
