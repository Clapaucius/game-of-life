﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitialStateGenerator : MonoBehaviour
{
    public GameObject[,] cells;
    public GameObject cellPrefab;
    public Camera mainCamera;

    [Range(0f, 1f)]
    [HideInInspector] public float chanceOfAliveCell;
    
    [Range(1, 150)]
    public int width;
    [Range(1, 150)]
    public int height;  

    public bool HasGameBegun { get; private set; }

    private void Start()
    {
        mainCamera.transform.position = new Vector3(width / 2f, height / 2f, mainCamera.transform.position.z);
        mainCamera.GetComponent<Camera>().orthographicSize = height / 2f + 1f; 
        CreateCells();
    }

    public void StartLivingProcess()
    {
        HasGameBegun = true;
    }
    
    public void StopLivingProcess()
    {
        HasGameBegun = false;
    }

    private void CreateCells()
    {
        cells = new GameObject[width, height];

        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                cells[i, j] = Instantiate(cellPrefab, new Vector2(i, j), Quaternion.identity);
            }
        }

        SetAllCellsDead();

    }

    public void SetAllCellsDead()
    {
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                cells[i, j].GetComponent<Cell>().SetDead();
            }
        }
    }

    public void SetCellStatesRandomly()
    {
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                if (UnityEngine.Random.value <= chanceOfAliveCell)
                {
                    cells[i, j].GetComponent<Cell>().SetAlive();
                }
                else
                {
                    cells[i, j].GetComponent<Cell>().SetDead();
                }
            }
        }
    }
}
